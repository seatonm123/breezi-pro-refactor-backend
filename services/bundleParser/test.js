
const Index = require('./index');

const tkn =
  `eyJraWQiOiJXMzZzRyt4V0hzeUllVXpZQzdtemJHUkJUN1FhcTArUjlQZXZ2enRpSFUwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI1MTIyNTJhMy03MzZhLTQ3NTEtOTZkZi00NjdiNGYzOWEzN2UiLCJldmVudF9pZCI6ImY3NWUxNzJmLWIxODMtNDFhMy1hNmYxLTkwMWE4MWNmZmU2NSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1ODAxOTc5MjcsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX0hKbnVzWGNnWiIsImV4cCI6MTU4MDIwMTUyNywiaWF0IjoxNTgwMTk3OTI3LCJqdGkiOiI0NGEyODZjZS1iZWJmLTRiZjgtYjk3Yi0wNGE1ZGY1MDU0OTUiLCJjbGllbnRfaWQiOiJzNTg5a2FjaWM4ajJkdjVuM2pmbnZnYWpjIiwidXNlcm5hbWUiOiI1MTIyNTJhMy03MzZhLTQ3NTEtOTZkZi00NjdiNGYzOWEzN2UifQ.qS80Pz2Grfi-JCMolgTnTy6RD6IO-Vu_PCWhHqApXJ0MDXmd_O7oXzur3Wml78aR82w-_YFObODmVc55m2uXX9XBiNLluTnW4kjDEqlUAA-6MqCaviSDpfb7zNs28oXtmUlk8tPyz0ZLIbou-GaYG0cLDOZu-EI-yFQ5x0xgxeTdb27fL1d5ZNKFGdUf3N8MqkowIwpImE6BLeqzHUsO_tLlPsP5S6qcoFyMekehLzJhZFkEgjtBpRUaOO0kZl8Qa55ZkB0vCpYS_foqLFHHg8tJu4yLNvuh_P16io-TjoDNcVOM-x4qQJChc61kDeMhXFuFfMIbekpMVDzSblY-pw`;

const TestEvent = {
  httpMethod: 'GET',
  pathParameters: {
    building: 'bb4d4fc0-fb47-402d-97f7-7dca31667004'
    // device: '5808485723212637'
  },
  queryStringParameters: {
    // floor: '10'
  },
  headers: {
    Authorization: tkn,
    'Content-Type': 'application/json'
  }
}

Index.handler(TestEvent, {}, (err, res) => {
  err ? console.log({ERROR: err}) : console.log(res);
});
