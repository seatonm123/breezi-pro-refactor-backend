
const Thresh = require('./thresholds');

const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();
const moment = require('moment');

exports.Bundle = class Bundle {

  constructor(org, admin){
    this.buildings = [];
    this.admin = admin;
    this.org = org;
    this.team = {};
  }

  async getTeam(){
    let team = [this.admin];
    await Promise.all(this.org.admins.map(async user => {
      let teamMember = await Dynamo.getItem({
        TableName: 'Users-matt',
        Key: {
          email: user
        }
      }).promise();
      if (user.Item && user.Item.email) {
        team.push(user.Item);
      }
    }));
    this.team = team;
  }

  async getBldgData(){
    let bldgBundle = {};
    let alerts = new Alerts();
    let avg = new Avg();
    if (!this.org.buildings.length) {
      this.buildings = {
        alerts: alerts,
        avg: avg,
        active: 0,
        bldgs: [],
        overallAlerts: {
          bad: 0,
          crit: 0
        }
      }
    } else {
      let bldgs = await Promise.all(this.org.buildings.map(async b => {
        b = await compileBldg(b);
        return Promise.resolve(b);
      }));
      this.buildings = bldgs;
    }
  }

};

async function compileBldg(bId){
  let bldg = new exports.Building();
  await bldg.bldgDetails(bId);
  return bldg;
}

exports.Building = class Building {

  constructor(){
    this.floors = [];
    this.alerts = new Alerts();
    this.avg = new Avg();
    this.overallAlerts = {bad: 0, crit: 0};
  }

  async bldgDetails(id){
    let bldg = await Dynamo.getItem({
      TableName: 'Buildings-matt',
      Key: {
        id: id
      }
    }).promise();
    if (!bldg.Item || !bldg.Item.id) {
      return Promise.resolve(null);
    }
    let building = bldg.Item;
    this.info = {
      street: building.street,
      streetExtended: building.streetExtended,
      city: building.city,
      altCity: building.altCity,
      state: building.state,
      zipCode: building.zipCode,
      country: building.country,
      name: building.name,
      type: building.type,
      id: building.id
    };
    await this.setFloorData(building);
    this.avg = exports.totalAvg(this.floors);
    if (this.floors.length) this.alerts = exports.prevAlerts(this.floors);
    this.overall();
  }

  async setFloorData(b){
    let len = (b.floors.aboveGround + b.floors.belowGround + 1);
    let floors = Array.from({length:len},(v,k)=>k+1);
    floors = floors.map(f => b.floors.aboveGround - (f - 1));
    floors = floors.map(f => {
      let floor = new exports.Floor(f);
      let bFloor = b.floors.active.find(fl => fl.index === f);
      if (bFloor !== undefined) floor.info.name = bFloor.name;
      return floor;
    });

    if (b.airVents.length) {
      let rooms = await Promise.all(b.airVents.map(async r => {
        let room = new exports.Room(r);
        await room.getAvg();
        await room.getDays();
        room.parseThresh();
        room.overall();
        return Promise.resolve(room);
      }));
      rooms.forEach(r => {
        let floor = floors.find(f => f.info.index == r.info.floorIndex);
        let fIndex = floors.indexOf(floor);
        floor.rooms.push(r);
        floors[fIndex] = floor;
      });
    }
    floors.forEach(f => {
      if (f.rooms && f.rooms.length) f.active = true;
      f.avg = exports.totalAvg(f.rooms);
      if (f.rooms && f.rooms.length) f.alerts = exports.prevAlerts(f.rooms);
      f.overall();
    });
    this.floors = floors;
  }

  overall(){
    Object.keys(this.alerts).forEach(k => {
      if (this.alerts[k].bad > 0) {
        this.overallAlerts.bad += this.alerts[k].bad;
      }
      if (this.alerts[k].crit > 0) {
        this.overallAlerts.crit += this.alerts[k].crit;
      }
    });
  }

};

exports.Floor = class Floor {

  constructor(i){
    this.active = false;
    this.alerts = new Alerts();
    this.avg = new Avg();
    this.info = {
      index: i,
      active: false
    };
    this.overallAlerts = {bad: 0, crit: 0};
    this.rooms = [];
  }

  overall(){
    Object.keys(this.alerts).forEach(k => {
      if (this.alerts[k].bad > 0) {
        this.overallAlerts.bad += this.alerts[k].bad;
      }
      if (this.alerts[k].crit > 0) {
        this.overallAlerts.crit += this.alerts[k].crit;
      }
    });
  }

};

exports.Room = class Room {

  constructor(r){
    this.active = r.active;
    this.info = {
      airFilters: r.airFilters,
      altSize: r.altSize,
      altConfig: r.altConfig,
      device: r.device,
      fBrand: r.fBrand,
      fModel: r.fModel,
      fType: r.fType,
      hvacName: r.hvacName,
      id: r.id,
      manufacturer: r.manufacturer,
      modelNumber: r.modelNumber,
      floorIndex: r.floorIndex
    }
    this.alerts = new Alerts();
    this.avg = new Avg();
    this.overallAlerts = {bad: 0, crit: 0};
    this.lastChanged = calcChanged(r);
    this.lastUpdated = -1;
    this.hasDap = false;
  }

  async getAvg(){
    if (this.active) {
      let params = {
        TableName: 'Measurements-matt',
        KeyConditionExpression: ':did = did',
        ExpressionAttributeValues: {
          ':did': this.info.device
        },
        ScanIndexForward: false,
        Limit: 1
      }
      let meas = await Dynamo.query(params).promise();
      if (meas.Items && meas.Items[0] && meas.Items[0].tms) {
        meas = meas.Items[0];
        this.lastUpdated = momentify(meas.tms + (meas.ctr * meas.per), true);
        let props = ['ttp', 'hhm', 'iaq', 'co2'];
        let self = this;
        props.forEach(p => {
          if (meas.dat[p]) {
            self.avg[p] = parseInt(meas.dat[p][meas.dat[p].length - 1].toFixed());
          }
        });
      }
    }
  }

  async getDays(){
    let filter = this.info.airFilters.find(f => !f.toTimestamp);
    if (filter) {
      let params = {
        TableName: 'DataAnalysisPackets-matt',
        KeyConditionExpression: 'dataAnalysisPK = :dap',
        ExpressionAttributeValues: {
          ':dap': filter.id + '.DAILY_AIR_FILTER'
        },
        ScanIndexForward: false,
        Limit: 1
      }
      let dap = await Dynamo.query(params).promise();
      if (dap.Items && dap.Items.length) {
        this.hasDap = true;
        dap = dap.Items[0];
        this.avg.days = dap.values.lifeExpectancy;
      }
    }
  }

  parseThresh(){
    Object.keys(this.avg).map(k => {
      if (
        (this.avg[k] > Thresh[k].bad.low && this.avg[k] < Thresh[k].bad.high)
        || (Thresh[k].bad2 && this.avg[k] > Thresh[k].bad2.low && this.avg[k] < Thresh[k].bad2.high)
      ) {
        this.alerts[k].bad++
      }
      if (
        (this.avg[k] > Thresh[k].crit.low && this.avg[k] < Thresh[k].crit.high)
        || (Thresh[k].crit2 && this.avg[k] > Thresh[k].crit2.low && this.avg[k] < Thresh[k].crit2.high)
      ) {
        this.alerts[k].crit++;
      }
    });
  }

  overall(){
    Object.keys(this.alerts).forEach(k => {
      if (this.alerts[k].bad > 0) {
        this.overallAlerts.bad++;
      }
      if (this.alerts[k].crit > 0) {
        this.overallAlerts.crit++
      }
    });
  }

};

function calcChanged(room, mins){
  let filter = room.airFilters.find(f => !f.toTimestamp);
  if (filter == undefined) {
    return '-';
  }
  let tms = filter.fromTimestamp;
  return momentify(tms, mins);
}
function momentify(tms, mins){
  let momentified = moment(tms * 1000).format('MM-DD YYYY @ h:mma');
  let splitMonth = momentified.split('-');
  splitMonth[0] = monthInLeters(splitMonth[0]);
  if (splitMonth[1][0] == '0') {
      splitMonth[1] = splitMonth[1].substr(1, splitMonth[1].length - 1);
  }
  let withMonth = splitMonth.join('-');
  return mins
      ? withMonth.replace('@', 'at')
      : withMonth.split('@')[0].trim();
}
function monthInLeters(m){
    return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][parseInt(m) - 1];
}

exports.totalAvg = (arr) => {
  let rAvg = arr.map(r => r.avg);
  let fAvg = rAvg.reduce((a, i, x, r) => {
    Object.keys(i).forEach(k => {
      if (parseInt(i[k])) {
        if (a[k].val === '-') {
          a[k].val = i[k];
          a[k].count = 1;
        } else {
          a[k].val += i[k];
          a[k].count++;
        }
      }
    });
    if (x === r.length - 1) {
      Object.keys(a).forEach(k => {
        if (parseInt(a[k].val)) {
          a[k] = Math.round(a[k].val/a[k].count);
        } else {
          a[k] = '-';
        }
      })
    }
    return a;
  }, {
    days: {val: '-', count: 0},
    ttp: {val: '-', count: 0},
    hhm: {val: '-', count: 0},
    iaq: {val: '-', count: 0},
    co2: {val: '-', count: 0}
  });
  return fAvg;
}

exports.prevAlerts = (prev) => {
  return prev.reduce((a, i) => {
    Object.keys(i.alerts).forEach(k => {
      a[k].bad += i.alerts[k].bad;
      a[k].crit += i.alerts[k].crit;
    });
    return a;
  }, new Alerts());
}

class Alerts {
  constructor(){
    this.days =  {
      bad: 0,
      crit: 0
    };
    this.ttp = {
      bad: 0,
      crit: 0
    };
    this.hhm = {
      bad: 0,
      crit: 0
    };
    this.iaq = {
      bad: 0,
      crit: 0
    };
    this.co2 = {
      bad: 0,
      crit: 0
    }
  }
}

class Avg {
  constructor(){
    this.days = '-';
    this.ttp = '-';
    this.hhm = '-';
    this.iaq = '-';
    this.co2 = '-';
  }
}
