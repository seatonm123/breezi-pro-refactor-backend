
// local imports
const Core = require('./core');
const Cls = require('./classes');

// npm dependencies and configs
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': process.env.ORIGIN559 || '*'
      }
    })
  }

  if (!event.headers.Authorization) {
    response(401, 'AccessToken must be present in request Authorization header');
  }

  if (event.httpMethod !== 'GET') {
    response(405, 'HTTP method not allowed');
  }

  Core.getUserOrg(event.headers.Authorization)
    .then(details => {
      const admin = details.user;
      const org = details.org;
      let params  = parseParams();
      switch (params.type) {
        case 'building':
          if (org.buildings.includes(params.id)) {
            getBuildingBundle(params.id);
          } else {
            response(400, `Building with id ${params.id} belongs to a different organization`);
          }
        break;
        case 'floor':
          if (org.buildings.includes(params.id)) {
            let bId = params.id;
            let fIndex = params.index;
            getFloorBundle(bId, fIndex);
          } else {
            response(400, `Building with id ${params.id} belongs to a different organization`);
          }
        break;
        case 'device':
          getDevBundle(params.id);
        break;
        default:
          getFullBundle(org, admin);
      }
    }).catch(err => {
      response(err.code, err.msg);
    });

  function parseParams(){
    if (!event.pathParameters) {
      return {};
    } else {
      if (event.queryStringParameters) {
        try {
          return {type: 'floor', id: event.pathParameters.building, index: event.queryStringParameters.floor}
        } catch(err) {
          response(422, 'Request for floor bundle requires buildingId in request body');
        }
      } else {
        if (event.pathParameters.building) {
          return {type: 'building', id: event.pathParameters.building};
        } else {
          return {type: 'device', id: event.pathParameters.device};
        }
      }
    }
  }

  async function getBuildingBundle(id){
    try {
      let bldg = new Cls.Building();
      await bldg.bldgDetails(id);
      console.log(bldg.alerts, bldg.overallAlerts, bldg.avg);
      response(200, bldg);
    } catch(err) {
      response(400, `Builidng with id ${id} not found`);
    }
  }

  async function getFloorBundle(bldg, flr){
    let building = await Dynamo.getItem({
      TableName: 'Buildings-matt',
      Key: {
        id: bldg
      }
    }).promise();
    if (!building.Item || !building.Item.id) {
      response(400, `Building with id ${bldg} not found`);
    }
    primeRooms(building.Item, flr);
  }

  function primeRooms(bldg, flr){
    let bFloor = bldg.floors.active.find(f => f.index == flr);
    if (bFloor === undefined) {
      response(400, `Building with id ${bldg.id} does not contain floor ${flr}`);
    }
    let floor = new Cls.Floor();
    floor.info.name = bFloor.name;
    floor.info.index = flr;
    floor.active = true;
    if (!bFloor.rooms || !bFloor.rooms.length) {
      response(400, `Floor ${flr} does not contain any active rooms`);
    }
    let rooms = bFloor.rooms.map(r => {
      try {
        return bldg.airVents.find(v => v.id === r);
      } catch(err) {
        return null;
      }
    });
    rooms = rooms.filter(r => r !== null);
    if (!rooms.length) {
      response(400, `Floor ${flr} does not contain any active rooms`);
    }
    getFloorMeas(floor, rooms);
  }

  async function getFloorMeas(floor, rooms){
    rooms = await Promise.all(rooms.map(async r => {
      let room = new Cls.Room(r);
      await room.getAvg();
      await room.getDays();
      room.parseThresh();
      room.overall();
      return Promise.resolve(room);
    }));
    floor.rooms = rooms;
    if (!floor.rooms && !floor.rooms.length) floor.active = false;
    floor.avg = Cls.totalAvg(floor.rooms);
    floor.alerts = Cls.prevAlerts(floor.rooms);
    floor.overall();
    response(200, floor);
  }

  async function getDevBundle(dev){
    console.log(dev);
    let device = await Dynamo.getItem({
      TableName: 'Devices-matt',
      Key: {
        deviceId: parseInt(dev)
      }
    }).promise();
    if (!device.Item || !device.Item.deviceId) {
      response(400, `Device ${dev} not found`);
    }
    let bldgDev = bldgByDev(device.Item);
  }

  async function bldgByDev(dev){
    let currentVent = dev.airVentAssignments.find(a => !a.toTimestamp);
    if (currentVent === undefined) {
      response(400, `Device ${dev} is not currently assigned to an airVent`);
    }
    let bldg = await Dynamo.getItem({
      TableName: 'Buildings-matt',
      Key: {
        id: currentVent.buildingId
      }
    }).promise();
    if (!bldg.Item || !bldg.Item.id) {
      response(400, `Assigned building for device ${dev.deviceId} not found`);
    }
    bldg = bldg.Item;
    let bRoom = bldg.airVents.find(v => v.device === dev.deviceId);
    if (bRoom === undefined) {
      response(400, `Assigned room for device ${dev.deviceId} not found`);
    }
    try {
      let room = new Cls.Room(bRoom);
      await room.getAvg();
      await room.getDays();
      room.parseThresh();
      room.overall();
      response(200, room)
    } catch(err) {
      console.log(err);
      response(400, err.message ? err.message : 'Error loading room data');
    }
  }

  async function getFullBundle(org, admin){
    let bundle = new Cls.Bundle(org, admin);
    await bundle.getTeam();
    try {
      await bundle.getBldgData();
      response(200, bundle);
    } catch(err) {
      console.log(err);
    }
  }

};
