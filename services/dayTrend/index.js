
const Core = require('./core');
const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': process.env.ORIGIN || '*'
      }
    });
  }

  if (event.httpMethod !== 'GET') {
    response(403, 'HTTP method not allowed');
  }

  if (!event.headers.Authorization) {
    response(401, 'AccessToken must be present in request Authorization header');
  }

  if (!event.pathParameters || !event.pathParameters.deviceId) {
    response(422, 'Invoked url must include deviceId pathParameter');
  }

  if (!event.queryStringParameters || !event.queryStringParameters.from || !event.queryStringParameters.to) {
    response (422, 'Invoked url must include TO and FROM queryStringParameters');
  }

  const tkn = event.headers.Authorization;
  const deviceId = parseInt(event.pathParameters.deviceId);
  const from = parseInt(event.queryStringParameters.from);
  const to = parseInt(event.queryStringParameters.to);

  Core.getUserOrg(tkn)
    .then(async details => {
      let owned = await validateUserOwnsDevice(details.user, details.org);
      if (owned.err) {
        response(owned.err.code, owned.err.msg);
      }
      getMeasForRange(owned);
    })
    .catch(err => {
      response(err.code, err.msg);
    });

  function validateUserOwnsDevice(admin, org){
    return new Promise(async (resolve, reject) => {
      let device = await Dynamo.getItem({
        TableName: 'Devices-matt',
        Key: {
          deviceId: deviceId
        }
      }).promise();
      if (!device.Item || !device.Item.deviceId) {
        resolve({err: {code: 400, msg: `Device ${deviceId} not found`}});
      }
      device = device.Item;
      if (device.userId === admin.id || findDevBldg(device, org)) {
        resolve(deviceId);
      } else {
        resolve({err: {code: 400, msg: `Device ${deviceId} belongs to a different organization`}});
      }
    });
  }

  function findDevBldg(device, org){
    let currentAssignment = device.airVentAssignments.find(a => !a.toTimestamp);
    return (currentAssignment == undefined || !currentAssignment.buildingId || !org.buildings.contains(currentAssignment.buildingId)) ? false : true;
  }

  async function getMeasForRange(did){
    let params = {
      TableName: 'Measurements-matt',
      KeyConditionExpression: 'did = :did AND tms BETWEEN :from AND :to',
      ExpressionAttributeValues: {
        ':did': did,
        ':from': from,
        ':to': to
      }
    };
    console.log(params);
    Dynamo.query(params).promise()
      .then(measurements => {
        if (!measurements.Items) {
          throw null;
        }
        response(200, measurements.Items);
      }).catch(err => {
        response(400, 'Measurements not found for specified range');
      });
  }

}
