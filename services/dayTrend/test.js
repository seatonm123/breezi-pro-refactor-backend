const Index = require('./index');

const tkn =
  `eyJraWQiOiJXMzZzRyt4V0hzeUllVXpZQzdtemJHUkJUN1FhcTArUjlQZXZ2enRpSFUwPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI1MTYwMjI1Ny05MGUwLTRlMjktOTIzYi0wNGI2ZDZkOWM3OGUiLCJldmVudF9pZCI6ImFiZjJjNTljLWZiOTUtNDU4My1hYmViLWMzMmUwNjlmZTVmNyIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE1Nzk4MjI4NjgsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX0hKbnVzWGNnWiIsImV4cCI6MTU3OTgyNjQ2OCwiaWF0IjoxNTc5ODIyODY4LCJqdGkiOiI4N2EzNzExNS1hMTI1LTRlOWItYTU3OS05MGUyZWZkYzQ2ZDgiLCJjbGllbnRfaWQiOiJzNTg5a2FjaWM4ajJkdjVuM2pmbnZnYWpjIiwidXNlcm5hbWUiOiI1MTYwMjI1Ny05MGUwLTRlMjktOTIzYi0wNGI2ZDZkOWM3OGUifQ.BpqLJ7NyGMEsasckOCStWsWbIvFRHT0TKUDVRz7kWtUbW4Q791JyByVQ-G7FavYbttC1mwyhw77Q0159jfB1fDUgq3NR5pRs3ynQIhYImKb-soYnqWfdbrYqY98BmYItOd5WLcTZLm8YliawQs1auIIgIuR5kI4ANCUPu7mLNlcgCsfXPT1Zh8QWWyCkC_EIELbGCMKXeaodvSflVqQWQDbe1U1UJpWW0LODJZwzOQiQijmOpFztxt9HbV34_VEdMvidzdwHms4v7TUMFDtCFMW2siWJiKblsF5xa2n17sn6WKfWonx_jjPpZ00bbfOXLnTbD47aETkOLYEUgFrAqA`;
const did = 3777267609357006;
const from = Math.floor(new Date('2019-12-10')/1000);
const to = Math.floor(new Date('2019-12-11')/1000);

const TestEvent = {
  httpMethod: 'GET',
  headers: {
    Authorization: tkn,
    'Content-Type': 'application/json'
  },
  pathParameters: {
    deviceId: did.toString()
  },
  queryStringParameters: {
    from: from.toString(),
    to: to.toString()
  }
}

Index.handler(TestEvent, {}, (err, res) => {
  err ? console.log({ERROR: err}) : console.log(JSON.parse(res.body).length);
});
